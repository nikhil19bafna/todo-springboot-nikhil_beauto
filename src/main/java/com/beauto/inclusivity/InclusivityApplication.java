package com.beauto.inclusivity;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.beauto.inclusivity")
@EntityScan("com.beauto.inclusivity.model")
public class InclusivityApplication {
public static void main(String[] args) {
SpringApplication.run(InclusivityApplication.class, args);
}
}
