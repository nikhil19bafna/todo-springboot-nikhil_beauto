package com.beauto.inclusivity.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.beauto.inclusivity.model.Task;
import com.beauto.inclusivity.model.User;
import com.beauto.inclusivity.repository.TaskRepository;
import com.beauto.inclusivity.repository.UserRepository;


@RestController
@RequestMapping(value = "/api/user/", produces = "application/json")
public class TaskController {

	@Autowired
	TaskRepository taskRepository;
	
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(method = RequestMethod.POST, value = "{id}/task")
	public Task createTask(@PathVariable long id, @RequestBody Task task) throws Exception
	{
		Task saveTask = new Task();
		Optional<User> optional = userRepository.findById(id);
		if (optional.isPresent()) {
			    task.setId(id);
				saveTask = taskRepository.save(task);
			
		} else {
			throw new Exception("Optional is Empty");
		}
		return saveTask;
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/task/{taskId}", consumes = "application/json")
	public Task updationTask(@PathVariable long id, @PathVariable long taskId, @RequestBody Task task)
			throws Exception {
		Task updateTask = new Task();
		User optional = userRepository.findById(id).orElseThrow(() -> new RuntimeException("No data!"));
		System.out.println(optional.toString());
		Task taskOptional = taskRepository.findByTaskId(taskId).orElseThrow(() -> new RuntimeException("No data!"));
		
		
		System.out.println(taskOptional.toString());
		if(optional.getId() == id & taskOptional.getTaskId() == taskId)
		{
			task.setDateTime(task.getDateTime());
			task.setDescription(task.getDescription());
			task.setName(task.getName());
			task.setTaskId(taskId);
			task.setId(id);
			updateTask = taskRepository.save(task);
		} 
		return updateTask;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}/task/{taskId}")
	public boolean deletetask(@PathVariable long id, @PathVariable long taskId) {
		boolean flag =true;
		User optional = userRepository.findById(id).orElseThrow(() -> new RuntimeException("No data!"));
		Task taskOptional = taskRepository.findByTaskId(taskId).orElseThrow(() -> new RuntimeException("No data!"));
		if (id == optional.getId() & taskOptional.getTaskId() == taskId) {
			taskRepository.delete(taskOptional);
			flag =true;
			return flag;
		}
		flag=false;
		return flag;
	}

   
    		
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/task/{taskId}")
    public Optional<Task> getAllTaskInfo(@PathVariable long id, @PathVariable long taskId)
    {	Optional<Task> task = taskRepository.findByTaskId(taskId);	
		Optional<User> user = userRepository.findById(id);
		if(user.isPresent()) 
		{
				return task;
		}
	 return null;
	}
	
	
	@RequestMapping(method = RequestMethod.GET,value = "/{id}/task")
    public List<Task> getTaskforUser(@PathVariable long id)
    {
		List<Task> task = taskRepository.findAllById(id);
	    return task;
	}
}
