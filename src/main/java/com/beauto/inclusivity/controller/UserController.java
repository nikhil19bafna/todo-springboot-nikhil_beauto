package com.beauto.inclusivity.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beauto.inclusivity.model.User;
import com.beauto.inclusivity.repository.UserRepository;

@RestController
@RequestMapping(value = "/api/user", produces = "application/json")
public class UserController {
@Autowired
UserRepository userRepository;

@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
public User createUser(@RequestBody @Valid User user) {
return userRepository.save(user);
}

@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json")
public User updateUser(@RequestBody User user, @PathVariable long id) throws Exception {
	User userResponse = userRepository.findById(id).orElseThrow(Exception::new);
	userResponse.setFirstName(user.getFirstName());
	userResponse.setLastName(user.getLastName());
	return userRepository.save(userResponse);
}

@RequestMapping(method = RequestMethod.GET)
public Iterable<User> findUserAll(){
	return userRepository.findAll();
}

@RequestMapping(value = "/{id}", method = RequestMethod.GET)
public Optional<User> findUserID(@PathVariable long id) {
	return userRepository.findById(id);
}

}
