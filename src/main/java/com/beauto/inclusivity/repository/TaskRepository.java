package com.beauto.inclusivity.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.beauto.inclusivity.model.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long>{
	
	
	List<Task> findById(long id);

	Optional<Task> findByName(String name);

	Optional<Task> findByTaskId(long taskId);

	List<Task> findAllById(long id);


	
}
