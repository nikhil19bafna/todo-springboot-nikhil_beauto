package com.beauto.inclusivity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@ToString
public class User {
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull(message = "UserName can not be Null")
	@Column(name = "username", nullable = false)
	private String userName;
	
    @Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;

	public User(String userName, String firstName, String lastName) {
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
	}

}
