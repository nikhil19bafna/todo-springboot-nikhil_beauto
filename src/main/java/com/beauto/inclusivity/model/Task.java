package com.beauto.inclusivity.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@NoArgsConstructor
@ToString
public class Task {
	@Id
	@GeneratedValue
	private long taskId;
	private String name;
	private String description;
	private LocalDateTime dateTime;
	private long id;
	
	public Task(String name, String description, LocalDateTime dateTime) {
		this.name = name;
		this.description = description;
		this.dateTime = dateTime;
	}
}
