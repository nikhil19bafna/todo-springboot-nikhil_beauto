package com.beauto.inclusivity;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.beauto.inclusivity.model.Task;
import com.beauto.inclusivity.model.User;
import com.beauto.inclusivity.repository.TaskRepository;
import com.beauto.inclusivity.repository.UserRepository;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InclusivityApplicationTests {

	@Value("${local.server.port}")
	protected int serverPort;

	@Autowired
	UserRepository userRepository;

	@Autowired
	TaskRepository taskRepository;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;
	}

	@Test
	public void createUserEndPointTest() {
		String userName = "jsmith";
		String firstName = "John";
		String lastName = "Smith";
		User user = new User(userName, firstName, lastName);
		given().body(user).contentType(ContentType.JSON).when().post("/api/user").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue()))
				.body("firstName", is(firstName)).body("lastName", is(lastName)).body("userName", is(userName));
		Optional<User> userOnRecord = userRepository.findByUserName(userName);
		assertTrue(userOnRecord.isPresent());
	}

	@Test
	public void createUserNameUnsuccessfulTest() {
		String userName = null;
		String firstName = "Johny";
		String lastName = "Smithy";
		User user = new User(userName, firstName, lastName);
		given().body(user).contentType(ContentType.JSON).when().post("/api/user").then()
				.statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
		Optional<User> userOnRecord = userRepository.findByFirstName(firstName);
		assertFalse(userOnRecord.isPresent());
	}

	@Test
	public void updateUserSuccessfullyTest() {
		String firstName = "John";
		String userName = "jsmith";
		String updatedLastName = "Walker";
		User user = new User(userName, firstName, updatedLastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		System.out.println("user created : " + userRecord.get().toString());

		Long id = userRecord.get().getId();
		String updatedLname = "Smith";
		user.setLastName(updatedLname);

		given().body(user).contentType(ContentType.JSON).when().put("/api/user/" + id).then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue()))
				.body("firstName", is(firstName)).body("lastName", is(updatedLname)).body("userName", is(userName));
		userRecord = userRepository.findByUserName(userName);
		assertEquals(firstName, userRecord.get().getFirstName());
		assertEquals(updatedLname, userRecord.get().getLastName());
	}

	@Test
	public void updateUserUnsuccessfullyTest() {
		String firstName = "John";
		String userName = "jsmith";
		String updatedLastName = "Walker";
		User user = new User(userName, firstName, updatedLastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		System.out.println("user created : " + userRecord.get().toString());
		Long id = userRecord.get().getId();
		String updatedLname = null;
		user.setLastName(updatedLname);
		given().body(user).contentType(ContentType.JSON).when().put("/api/user/" + id).then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue()))
				.body("firstName", is(firstName)).body("lastName", is(updatedLname)).body("userName", is(userName));
		userRecord = userRepository.findByUserName(userName);
		assertEquals(firstName, userRecord.get().getFirstName());
		assertEquals(updatedLname, userRecord.get().getLastName());
	}

	@Test
	public void findByAllTest() {
		given().contentType(ContentType.JSON).when().get("/api/user").then().statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	public void findbyidTest() {
		String firstName = "Nikhil";
		String userName = "nbafna";
		String lastName = "Bafna";
		User userCheck = new User();
		long id = userCheck.getId();
		given().body(userCheck).contentType(ContentType.JSON).when().get("/api/user/" + id).then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	public void createTaskTest() {
		String userName = "nbafna";
		String firstName = "Nikhil";
		String lastName = "Bafna";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);

		String name = "test";
		String description = "test1";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		long taskId = 2;
		long id = 1;
		System.out.println(dateTime);
		Task task = new Task(name, description, dateTime);

		given().body(task).contentType(ContentType.JSON).when().post("/api/user/" + id + "/task").then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("taskId", not(nullValue()))
				.body("description", is(description)).body("name", is(name));

		Optional<Task> taskRecord = taskRepository.findByName(name);
		assertTrue(taskRecord.isPresent());
		System.out.println("Task : " + taskRecord.get().toString());
	}

	@Test
	public void createtaskFailTest() {
		String userName = "nbafna";
		String firstName = "Nikhil";
		String lastName = "Bafna";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);

		String name = "test";
		String description = "test1";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		long taskId = 2;
		long id = 5;
		System.out.println(dateTime);
		Task task = new Task(name, description, dateTime);
		if (userRepository.findById(id).isPresent())
			given().body(task).contentType(ContentType.JSON).when().post("/api/user/" + id + "/task").then()
					.statusCode(equalTo(HttpStatus.BAD_REQUEST.value()));
	}

	@Test
	public void updateTaskTest() {
		// Add user
		String userName = "Test Update";
		String firstname = "Nikhil";
		String lastname = "Bafna";
		User user = new User(userName, firstname, lastname);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		// Add task
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		Long id = userRecord.get().getId();
		task.setId(id);
		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		// Update Task
		Long taskid = taskRecord.get().getTaskId();
		String updatename = "Update Task updated";
		String updatedescription = "Update Task updated description";
		task.setName(updatename);
		task.setDescription(updatedescription);
		given().body(task).contentType(ContentType.JSON).when().put("/api/user/" + id + "/task/" + taskid).then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("taskId", not(nullValue()))
				.body("description", is(updatedescription)).body("name", is(updatename));
		taskRecord = taskRepository.findById(taskid);
		assertEquals(updatedescription, taskRecord.get().getDescription());
	}

	@Test
	public void updateTaskFailbyuserIDTest() {
		// Add user
		String userName = "Test Update";
		String firstname = "Nikhil";
		String lastname = "Bafna";
		User user = new User(userName, firstname, lastname);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		// Add task
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		Long id = 59l;
		task.setId(id);

		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		// Update Task
		Long taskid = taskRecord.get().getTaskId();
		String updatename = "Update Task updated";
		String updatedescription = "Update Task updated description";
		task.setName(updatename);
		task.setDescription(updatedescription);
		given().body(task).contentType(ContentType.JSON).when().put("/api/user/" + id + "/task/" + taskid).then()
				.statusCode(equalTo(HttpStatus.INTERNAL_SERVER_ERROR.value()));
	}

	@Test
	public void deleteTaskTest() {
		// Add user
		String userName = "Test Update";
		String firstName = "Nikhil";
		String lastName = "Bafna";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		// Add task
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		// Delete Task
		Long taskid = taskRecord.get().getTaskId();
		Long id = userRecord.get().getId();
		given().body(task).contentType(ContentType.JSON).when().delete("/api/user/" + id + "/task/" + taskid).then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	public void deleteTaskFailTest() {
		// Add user
		String userName = "Test Update";
		String firstName = "Nikhil";
		String lastName = "Bafna";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		long id = 5l;
		// Add task
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		long taskid = taskRecord.get().getTaskId();
		// Delete Task
		given().body(task).contentType(ContentType.JSON).when().delete("/api/user/" + id + "/task/" + taskid).then()
				.statusCode(equalTo(HttpStatus.INTERNAL_SERVER_ERROR.value()));
	}

	@Test
	public void getAlltaskInfoTest() {
		// Add user
		String userName = "Test Update";
		String firstName = "Nikhil";
		String lastName = "Bafna";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		long id = userRecord.get().getId();
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		long taskid = taskRecord.get().getTaskId();
		given().body(task).contentType(ContentType.JSON).when().get("/api/user/" + id + "/task/" + taskid).then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	public void getTaskforUserTest() {
		String userName = "Test Update";
		String firstName = "Pravin";
		String lastName = "Jain";
		User user = new User(userName, firstName, lastName);
		userRepository.save(user);
		Optional<User> userRecord = userRepository.findByUserName(userName);
		long id = userRecord.get().getId();
		String name = "Update Task";
		String description = "Update Task";
		LocalDateTime dateTime = LocalDateTime.of(2018, 12, 12, 3, 30);
		Task task = new Task(name, description, dateTime);
		taskRepository.save(task);
		Optional<Task> taskRecord = taskRepository.findByName(name);
		long taskid = taskRecord.get().getTaskId();
		given().body(task).contentType(ContentType.JSON).when().get("/api/user/" + id + "/task/").then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}
	
}
